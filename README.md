# Instruções

Os dados representam as relações de vendas de contratos de uma academia de musculação para seus clientes.

Desenvolva um script que leia os dados de `backup/backup-data.sql` e insira no banco referenciado em `database/db-tables.sql`.

Qualquer linguagem de programação pode ser utlizada para resolução do problema, para o banco de dados, utilize `MySQL`.

Os bancos têm regras de negócio diferentes para o mesmo problema (vender contratos de academia), por isso, os dados em `backup/backup-data.sql` devem
ser manipulados e tratados de maneira que seja possível inseri-los nas tabelas referenciadas em `database/db-tables.sql`.

Descreva o processo de implementação e as etapas necessárias para a resolução do problema em `comentarios.md`, se não conseguir implementar alguma etapa,descreva o passo a passo do que deve ser feito.

Caso tenha dúvida com alguma tabela ou atributo, verifique os comentários no arquivo `db-tables.sql`

As queries não precisam ser otimizadas.

Entregue o projeto final com um dump do banco descrito em `database/db-tables.sql` em um fork do repositório ou em zip por e-mail

#### Recomendações:
* Crie códigos para interligar dados de tabelas diferentes.
* Faça subqueries com `SELECT` caso necessite, na hora de inserir, de algum dado já inserido nas tabelas do banco descrito em `database/db-tables.sql`.

## Será avaliado:
* Legibilidade do código
* Resultado final dos dados inseridos no banco descrito em `database/db-tables.sql`
* Queries para receber os dados representados em `backup/backup-data.sql`
