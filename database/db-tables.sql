-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema teste
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `teste` DEFAULT CHARACTER SET utf8 ;
USE `teste` ;

-- -----------------------------------------------------
-- Table `teste`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teste`.`cliente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `email` VARCHAR(45) NULL,
  `cpf` VARCHAR(20) NULL,
  `codigo_importacao` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teste`.`contrato`
-- Um contrato é o plano que o cliente compra na academia
-- Exemplo:
-- Plano Trimestral Livre - 3X
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teste`.`contrato` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `valor_padrao` DOUBLE NULL,
  `numero_vigencia` INT NULL DEFAULT 1 COMMENT 'Quantidade de dias ou meses relacionado ao contrato',
  `tipo_vigencia` TINYINT NULL DEFAULT 0 COMMENT '0 - Por mês\n1 - Por dia',
  `codigo_importacao` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teste`.`cliente_venda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teste`.`cliente_venda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cliente_id` INT NOT NULL,
  `valor` DOUBLE NULL,
  `valor_desconto` DOUBLE NULL,
  `valor_aberto` DOUBLE NULL,
  `data_venda` TIMESTAMP NULL,
  `codigo_importacao` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cliente_venda_cliente_idx` (`cliente_id` ASC),
  CONSTRAINT `fk_cliente_venda_cliente`
    FOREIGN KEY (`cliente_id`)
    REFERENCES `teste`.`cliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teste`.`cliente_contrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teste`.`cliente_contrato` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `valor_contrato` DOUBLE NOT NULL,
  `data_inicio` TIMESTAMP NOT NULL,
  `data_vencimento` TIMESTAMP NOT NULL,
  `cliente_venda_id` INT NOT NULL,
  `contrato_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cliente_contrato_cliente_venda1_idx` (`cliente_venda_id` ASC),
  INDEX `fk_cliente_contrato_contrato1_idx` (`contrato_id` ASC),
  CONSTRAINT `fk_cliente_contrato_cliente_venda1`
    FOREIGN KEY (`cliente_venda_id`)
    REFERENCES `teste`.`cliente_venda` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_contrato_contrato1`
    FOREIGN KEY (`contrato_id`)
    REFERENCES `teste`.`contrato` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teste`.`lancamentos`
-- Recebimentos de determinada venda
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teste`.`lancamentos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tipo_recebimento` INT NOT NULL DEFAULT 1 COMMENT '1 - Dinheiro\n2 - Cartão de Débito\n3 - Cartão de Crédito\n4 - Boleto',
  `valor` DOUBLE ZEROFILL NOT NULL,
  `valor_desconto` DOUBLE NULL,
  `data_lancamento` TIMESTAMP NULL,
  `data_recebimento` TIMESTAMP NULL,
  `data_vencimento` TIMESTAMP NULL,
  `total_parcelas` INT NULL DEFAULT 1,
  `parcela_atual` INT NULL DEFAULT 1,
  `cliente_venda_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_recebimentos_cliente_venda1_idx` (`cliente_venda_id` ASC),
  CONSTRAINT `fk_recebimentos_cliente_venda1`
    FOREIGN KEY (`cliente_venda_id`)
    REFERENCES `teste`.`cliente_venda` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
